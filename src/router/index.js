import Vue from 'vue'
import Router from 'vue-router'
import pchome from '@/components/pc/home'


import mobilehome from '@/components/mobile/home'

Vue.use(Router)
export default new Router({
    routes: [{
            path: '/',
            name: 'home',
            component: pchome
        },
        {
            path: '/pc',
            name: 'pchome',
            component: pchome
        },
        {
            path: '/mobile',
            name: 'mobilehome',
            component: mobilehome
        }
    ]
})